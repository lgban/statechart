defmodule Stchart do
  use GenServer
  defstruct [
    current: nil,
    timer_ref: nil,
    timers: %{},
    transitions: %{}
  ]

  def start_link() do
    GenServer.start_link(__MODULE__, [])
  end

  def init(_) do
    {:ok, %Stchart{
      current: Off,
      timer_ref: nil,
      timers: %{
        {AlarmOn, 1} => AlarmOff,
        {AlarmOff, 1} => AlarmOn
      },
      transitions: %{
        {Off, :switch} => On,
        {On, :switch} => Off,
        {Off, :alarm} => AlarmOn,
        {AlarmOn, :switch} => Off,
        {AlarmOff, :switch} => Off,
      }
    }}
  end
  def handle_info({:alarm, new_state}, state) do
    IO.inspect "Moving to state '#{new_state}' -after alarm-"

    new_timer_ref =
      case Enum.find(state.timers, fn {{from, _}, _} -> from == new_state end) do
        {{_, time}, new_event} -> 
          Process.send_after(self(), {:alarm, new_event}, time * 1000)
        _ -> nil
      end
    
    {:noreply, state |> Map.put(:current, new_state) |> Map.put(:timer_ref, new_timer_ref)}
  end
  def handle_cast({:trigger, event}, %{current: current} = state) do
    if not Map.has_key?(state.transitions, {current, event}) do
      {:noreply, state}
    else
      if state.timer_ref do
        Process.cancel_timer(state.timer_ref)
      end

      new_state = Map.get(state.transitions, {current, event})
      IO.inspect "Moving to state '#{new_state}'"

      new_timer_ref =
        case Enum.find(state.timers, fn {{from, _}, _} -> from == new_state end) do
          {{_, time}, new_event} -> 
            Process.send_after(self(), {:alarm, new_event}, time * 1000)
          _ -> nil
        end
      
      {:noreply, state |> Map.put(:current, new_state) |> Map.put(:timer_ref, new_timer_ref)}
    end
  end

  def handle_call(:state, _from, %{current: current} = state) do
    {:reply, current, state}
  end
end
